package Ap.p4.qus.structuralpattern;

public class GirlsDress implements Dress {

	public String selectdress() {
		return "Girls Dress";
	}

	public double dressPrice() {
		return 1000.0;
	}
}
