package Ap.p4.qus.structuralpattern;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DecoratorPatternCustom {
	private static int Select;
	public static void main(String args[]) throws NumberFormatException, IOException {

		System.out.print("Dress Types\n");
		System.out.print("            1. Girls dress " + "  \n" 
		               + "            2. Boys Dress         \n" + "  "
				         + "          3. Exit               \n");
		System.out.print("Enter your choice: ");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Select = Integer.parseInt(br.readLine());

		switch (Select) {
		case 1: {
			GirlsDress Gd = new GirlsDress();
			System.out.println(Gd.selectdress());
			System.out.println(Gd.dressPrice());
		}
			break;

		case 2: {
			Dress DS = new BoysDress((Dress) new GirlsDress());
			System.out.println(DS.selectdress());
			System.out.println(DS.dressPrice());
		}
			break;

		default: {
			System.out.println("Other than these no Dress available");
		}

		}
	}
}
