package Ap.p4.qus.structuralpattern;

public class BoysDress extends DressDecorator {

	public BoysDress(Dress newDress) {
		super(newDress);
	}

	public String selectdress() {
		return super.selectdress() + " With Wedding Suits  ";
	}

	public double dressPrice() {
		return super.dressPrice() + 1500.0;
	}
}
