package Ap.p4.qus.structuralpattern;

public abstract class DressDecorator implements Dress {

	private Dress newDress;
	public DressDecorator(Dress newDress) {
		this.newDress = newDress;
	}

	@Override
	public String selectdress() {
		return newDress.selectdress();
	}

	@Override
	public double dressPrice() {

		return newDress.dressPrice();
	}

}
