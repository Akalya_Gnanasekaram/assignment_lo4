package Ap.p4.qus.structuralpattern;

public interface Dress {

	public String selectdress();
	public double dressPrice();
}
