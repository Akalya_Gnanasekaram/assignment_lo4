package Ap.p4.qus.behavioralpattern;

public class Build {
	private Strategy strgy;  
    
    public Build(Strategy strgy){  
       this.strgy = strgy;  
    }  

    public float executeStrategy(float num1, float num2){  
       return strgy.Computation(num1, num2);  
    }  

}
