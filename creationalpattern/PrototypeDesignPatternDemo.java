package Ap.p4.qus.creationalpattern;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class PrototypeDesignPatternDemo {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Staff Id: ");
		int staffid = Integer.parseInt(br.readLine());

		System.out.print("Enter Staff Name: ");
		String staffname = br.readLine();

		StaffRcd SRC = new StaffRcd(staffid, staffname);
		SRC.showRecord();

		// StaffRcd e2 = (EmployeeRcd) e1.getClone();
		// e2.showRecord();

	}

}
