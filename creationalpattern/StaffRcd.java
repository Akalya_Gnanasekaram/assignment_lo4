package Ap.p4.qus.creationalpattern;

class StaffRcd implements PrototypeDesignPattern{
	private int id;
	private String name;

	public StaffRcd() {
		System.out.println("\nStaff Records of National School ");
		System.out.println("---------------------------------");
		System.out.println("Staff id" + "\t" + "Staff name");

	}

	public StaffRcd(int id, String name) {

		this();
		this.id = id;
		this.name = name;

	}

	public void showRecord() {

		System.out.println(id + "\t\t" + name);
	}

	@Override
	public PrototypeDesignPattern getClone() {

		return new StaffRcd(id, name);
	}

}
